package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getList() throws ParserConfigurationException, IOException, SAXException {
        List<Flower> flowers = new ArrayList<Flower>();
        Flower flower = null;

        Schema schema = loadSchema("input.xsd");
        Document document = parseXmlDom(xmlFileName);
        document.getDocumentElement().normalize();

        try {
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlFileName)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        NodeList nList = document.getElementsByTagName("flower");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) node;

                flower = new Flower();

                flower.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                flower.setSoil(eElement.getElementsByTagName("soil").item(0).getTextContent());
                flower.setOrigin(eElement.getElementsByTagName("origin").item(0).getTextContent());
                flower.setStemColour(eElement.getElementsByTagName("stemColour").item(0).getTextContent());
                flower.setLeafColour(eElement.getElementsByTagName("leafColour").item(0).getTextContent());
                flower.setMeasureLen(eElement.getElementsByTagName("aveLenFlower").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setLenText(Integer.parseInt(eElement.getElementsByTagName("aveLenFlower").item(0).getTextContent()));
                flower.setMeasureTemp(eElement.getElementsByTagName("tempreture").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setTempText(Integer.parseInt(eElement.getElementsByTagName("tempreture").item(0).getTextContent()));
                flower.setLightRequiring(eElement.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getTextContent());
                flower.setMeasureWat(eElement.getElementsByTagName("watering").item(0).getAttributes().getNamedItem("measure").getTextContent());
                flower.setTextWat(Integer.parseInt(eElement.getElementsByTagName("watering").item(0).getTextContent()));
                flower.setMultiplying(eElement.getElementsByTagName("multiplying").item(0).getTextContent());

                flowers.add(flower);
            }
        }
        return flowers;
    }

    public void saveDocFromList(List<Flower> flowers, String xmlOutput) {
        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            documentFactory.setNamespaceAware(true);

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element flowersRoot = document.createElementNS("http://www.nure.ua", "flowers");
            document.appendChild(flowersRoot);

            for (Flower fl : flowers) {
                Element flower = document.createElement("flower");
                flowersRoot.appendChild(flower);

                Element name = document.createElement("name");
                name.appendChild(document.createTextNode(fl.name));
                flower.appendChild(name);

                Element soil = document.createElement("soil");
                soil.appendChild(document.createTextNode(fl.soil));
                flower.appendChild(soil);

                Element origin = document.createElement("origin");
                origin.appendChild(document.createTextNode(fl.origin));
                flower.appendChild(origin);

                Element visualParameters = document.createElement("visualParameters");
                flower.appendChild(visualParameters);

                //elements of visualParameters
                Element stemColour = document.createElement("stemColour");
                stemColour.appendChild(document.createTextNode(fl.stemColour));
                visualParameters.appendChild(stemColour);

                Element leafColour = document.createElement("leafColour");
                leafColour.appendChild(document.createTextNode(fl.leafColour));
                visualParameters.appendChild(leafColour);

                Element aveLenFlower = document.createElement("aveLenFlower");
                Attr measureLen = document.createAttribute("measure");
                measureLen.setValue(fl.measureLen);
                aveLenFlower.setAttributeNode(measureLen);
                aveLenFlower.appendChild(document.createTextNode(String.valueOf(fl.lenText)));
                visualParameters.appendChild(aveLenFlower);
                //end

                Element growingTips = document.createElement("growingTips");
                flower.appendChild(growingTips);

                //elements of growingTips
                Element tempreture = document.createElement("tempreture");
                Attr measureTemp = document.createAttribute("measure");
                measureTemp.setValue(fl.measureTemp);
                tempreture.setAttributeNode(measureTemp);
                tempreture.appendChild(document.createTextNode(String.valueOf(fl.tempText)));
                growingTips.appendChild(tempreture);

                Element lighting = document.createElement("lighting");
                Attr lightRequiring = document.createAttribute("lightRequiring");
                lightRequiring.setValue(fl.lightRequiring);
                lighting.setAttributeNode(lightRequiring);
                growingTips.appendChild(lighting);

                Element watering = document.createElement("watering");
                Attr measureWat = document.createAttribute("measure");
                measureWat.setValue(fl.measureWat);
                watering.setAttributeNode(measureWat);
                watering.appendChild(document.createTextNode(String.valueOf(fl.textWat)));
                growingTips.appendChild(watering);
                //end

                Element multiplying = document.createElement("multiplying");
                multiplying.appendChild(document.createTextNode(fl.multiplying));
                flower.appendChild(multiplying);

            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            Source domSource = new DOMSource(document);
            Result streamResult = new StreamResult(new FileWriter(new File(xmlOutput)));

            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Schema loadSchema(String name) {
        Schema schema = null;
        try {
            String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            SchemaFactory factory = SchemaFactory.newInstance(language);
            schema = factory.newSchema(new File(name));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return schema;
    }

    public static Document parseXmlDom(String name) {
        Document document = null;
        try {
            DocumentBuilderFactory factory
                    = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(new File(name));
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return document;
    }


}
