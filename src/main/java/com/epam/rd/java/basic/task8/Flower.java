package com.epam.rd.java.basic.task8;

public class Flower {
    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setText(int text) {
        this.lenText = text;
    }

    public void setMeasureLen(String measureLen) {
        this.measureLen = measureLen;
    }

    public void setLenText(int lenText) {
        this.lenText = lenText;
    }

    public void setMeasureTemp(String measureTemp) {
        this.measureTemp = measureTemp;
    }

    public void setTempText(int tempText) {
        this.tempText = tempText;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setMeasureWat(String measureWat) {
        this.measureWat = measureWat;
    }

    public void setTextWat(int textWat) {
        this.textWat = textWat;
    }

    public String name;
    public String soil;
    public String origin;
    public String stemColour;
    public String leafColour;
    public String measureLen;
    public int lenText;
    public String measureTemp;
    public int tempText;
    public String lightRequiring;
    public String measureWat;
    public int textWat;
    public String multiplying;

}









