package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);

		List<Flower> flowersDom = domController.getList();

		//sort	(case 1)
		flowersDom.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.name.compareTo(o2.name);
			}
		});
		
		// save
		String outputXmlFile = "output.dom.xml";

		domController.saveDocFromList(flowersDom, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory factory = SAXParserFactory.newInstance();

		List<Flower> flowersSax = new ArrayList<>();

		try (InputStream is = new FileInputStream(xmlFileName)) {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(is, saxController);

			flowersSax = saxController.getList();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		// sort  (case 2)
		flowersSax.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.soil.compareTo(o2.soil);
			}
		});
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.saveSAXDoc(flowersSax, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowersStax = staxController.getList();

		// sort  (case 3)
		flowersStax.sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.multiplying.compareTo(o2.multiplying);
			}
		});
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.saveSTAXDoc(flowersStax, outputXmlFile);
	}

}
